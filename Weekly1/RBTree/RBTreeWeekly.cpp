/**********************************************
* File: RBTreeTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the driver function for the solution to 
* the Weekly Coding Assignment for the Red-Black Tree
*
* Compilation Instructions:
* g++ -g -std=c++11 -Wpedantic RBTreeWeekly.cpp -o RBTreeWeekly 
* ./RBTreeWeekly
**********************************************/
#include "RBTree.h"

int totalProcesses = 10;
int longestLen = 1000;

struct Process{
	
	int procNum;
	int vruntime; 

	/********************************************
	* Function Name  : Process
	* Pre-conditions : int procNum, int vruntime
	* Post-conditions: none
	* 
	* Constructor for the Process 
	********************************************/
	Process(int procNum, int vruntime) : procNum(procNum), vruntime(vruntime) {}
	
	/********************************************
	* Function Name  : Process
	* Pre-conditions : none
	* Post-conditions: none
	* 
	* Empty Constructor for the Process 
	********************************************/
	Process() : procNum(0), vruntime(0) {}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Process& rhs
	* Post-conditions: bool
	* 
	* Overloaded < operator 
	********************************************/
	bool operator<(const Process& rhs) const{
		
		return vruntime < rhs.vruntime;
	}
	
	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Author& rhs
	* Post-conditions: bool
	* 
	* Overloaded == operator
	********************************************/
	bool operator==(const Process& rhs) const{
		
		return vruntime == rhs.vruntime;
	}
	
	/********************************************
	* Function Name  : operator!=
	* Pre-conditions : const Author& rhs
	* Post-conditions: bool
	* 
	* Overloaded == operator
	********************************************/
	bool operator!=(const Process& rhs) const{
		
		return vruntime != rhs.vruntime;
	}
	
	/********************************************
	* Function Name  : operator<<
	* Pre-conditions : std::ostream& outStream, const URN& printProcess
	* Post-conditions: friend std::ostream&
	* 
	* Overloaded friend function for Process 
	********************************************/
	friend std::ostream& operator<<(std::ostream& outStream, const Process& printProcess);
};

std::ostream& operator<<(std::ostream& outStream, const Process& printProcess){
	
	outStream << "(" << printProcess.procNum << ", " << printProcess.vruntime << ") ";
	
	return outStream;
}

/********************************************
* Function Name  : getProcesses
* Pre-conditions : RBTree<Process>* rbTree
* Post-conditions: none
*
* Generates totalProcesses procNum, vruntime pairs  
********************************************/
void getProcesses(RBTree<Process>* rbTree){
	
	// Seed the random number generator
	srand(time(NULL));
	
	std::cout << "Inserting " << totalProcesses << " processes into the scheduler" << std::endl;
	
	for(int i = 0; i < totalProcesses; i++){
		
		Process elem(i, rand()%longestLen);

		rbTree->insert(elem);
		
	}
	
	std::cout << "After insertion, the RBTree Processes are: " << std::endl;
	rbTree->printInOrder();
}

/********************************************
* Function Name  : getAndDeleteLeftMost
* Pre-conditions : RBTree<Process>* rbTree
* Post-conditions: none
* 
* Obtains the value of the leftmost node in the tree 
* and then deletes that value, akin to processing the 
* process with the smallest vruntime
********************************************/
void getAndDeleteLeftMost(RBTree<Process>* rbTree){
	
	// Check if the tree is empty
	if(rbTree->getRoot() == nullptr){
		std::cout << "The tree is empty" << std::endl;
		return;
	}
		
	// Get the root and set as the current "left most"
	RBTNode<Process> *leftMost = rbTree->getRoot();
	
	// Keep going down the leftmost until you can't get any further
	while(leftMost->left != nullptr)
		leftMost = leftMost->left;
	
	// Output the leftmost to the user
	std::cout << std::endl << "The leftmost process is " << leftMost->val << std::endl;
	
	// Delete the root by value 
	rbTree->deleteByVal(leftMost->val);
	
	// Print the output to the user
	std::cout << std::endl << "After deletion, the RBTree Processes are: " << std::endl;
	rbTree->printInOrder();
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver file for the program  
********************************************/
int main(int argc, char** argv) { 

  // Create the Red-Black Tree for the completely fair schedules
  RBTree<Process> cfs; 
  
  // Generate 10 processes with random vruntimes
  getProcesses(&cfs);
  
  // delete the leftmost node
  getAndDeleteLeftMost(&cfs);

  return 0; 
} 
